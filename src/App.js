import React, { useRef, useEffect, useCallback } from "react"
import "./App.css";
import Navbar from "./components/navbar/Navbar";
import Home from "./components/home/Home";

function App() {

  const ref = useRef()

  // The scroll listener
  const handleScroll = useCallback(() => {
    console.log("scrolling")
  }, [])

  // Attach the scroll listener to the div
  useEffect(() => {
    const div = ref.current
    div.addEventListener("scroll", handleScroll)
  }, [handleScroll])

  return (
    <div className="App" ref={ref}>
      <Navbar />
      <Home />
    </div>
  );
}

export default App;
