import React, { Component } from "react";
import { Slide } from "react-slideshow-image";
import "../../../../node_modules/react-slideshow-image/dist/styles.css";
import "./Carousel.css";

const slideImages = [
  {
    url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSIYioo7j7Vavu4WUv5lfiXuudej5FS54Q-CA&usqp=CAU",
    caption: "Slide 1"
  },
  {
    url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSIYioo7j7Vavu4WUv5lfiXuudej5FS54Q-CA&usqp=CAU",
    caption: "Slide 2"
  },
  {
    url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSIYioo7j7Vavu4WUv5lfiXuudej5FS54Q-CA&usqp=CAU",
    caption: "Slide 3"
  }
];

export class Carousel extends Component {
  render() {
    return (
      <Slide>
        {slideImages.map((slideImage, index) => (
          <div className="each-slide" key={index}>
            <div style={{ backgroundImage: `url(${slideImage.url})` }}>
              <span>{slideImage.caption}</span>
            </div>
          </div>
        ))}
      </Slide>
    );
  }
}

export default Carousel;
