import React, { Component } from "react";
import "./Home.css";
import Web from "../images/Web.png";
import Carousel from './carousel/Carousel';

export class Home extends Component {
  static propTypes = {};

  render() {
    return (
      <div className="home">
        <div className="image">
          <img src={Web} alt="" />
        </div>
        <div className="content">
          <h1>Lorem Ipsum</h1>
          <h2>Qu'est-ce que le Lorem Ipsum?</h2>
          <p>
            Le Lorem Ipsum est simplement du faux texte employé dans la
            composition et la mise en page avant impression. Le Lorem Ipsum est
            le faux texte standard de l'imprimerie depuis les années 1500.
          </p>
          <form className="subscribe">
            <input type="email" placeholder="your email.." />
            <button className="btn btn-subscribe">S'abonner</button>
          </form>
        </div>
      </div>
    );
  }
}

export default Home;
