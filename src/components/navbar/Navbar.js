import React, { useState } from "react";
import { FaGithub, FaFacebookF, FaGripVertical } from 'react-icons/fa';
import "./Navbar.css";

export default function Navbar() {
  const [isActive, setActive] = useState(false);

  const toggleClass = () => {
    setActive(!isActive);
  };

  return (
    <>
      <nav>
        <a className="logo" href="#">
          AR.
        </a>
        <ul className={isActive ? "navbar show" : "navbar"}>
          <li className="nav-item">
            <a className="nav-link" href="#">
              Courses
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="#">
              projects
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="#">
              contact us
            </a>
          </li>
        </ul>
        <div className="menu-btn">
          <button type="button" className="btn">
            <FaFacebookF />
          </button>
          <button type="button" id="searchBtn" className="btn search-btn">
            <FaGithub />
          </button>
          <button
            type="button"
            onClick={toggleClass}
            id="menu-response"
            className="btn search-btn"
          >
            <FaGripVertical />
          </button>
        </div>
      </nav>
    </>
  );
}
